package Shop_Lancuch_zobowiazan;

import java.util.Arrays;
import java.util.List;

public class LocalShopHandler extends ShopHandler {
    @Override
    protected List<String> getProducts() {
        return Arrays.asList("Samsung S10",
                "Huawei Mate 20");
    }

    @Override
    public String toString() {
        return "LocalShop";
    }
}
