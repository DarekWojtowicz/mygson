package Shop_Lancuch_zobowiazan;

public class ShopRequest {
    protected String request;

    public ShopRequest(String request) {
        this.request = request;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
