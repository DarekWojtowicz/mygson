package Shop_Lancuch_zobowiazan;

import java.util.Arrays;
import java.util.List;

public class WholesaleHandler extends ShopHandler {
    @Override
    protected List<String> getProducts() {
        return Arrays.asList("Xiaomi mi9",
                "Nokia 3210",
                "OnePlus 6T");
    }

    @Override
    public String toString() {
        return "Wholesale";
    }
}
