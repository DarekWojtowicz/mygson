package Shop_Lancuch_zobowiazan;

import java.util.Arrays;
import java.util.List;

public class CentralShopHandler extends ShopHandler {
    @Override
    protected List<String> getProducts() {
        return Arrays.asList("LG v30",
                "Motorola Z3",
                "Honor 20");
    }

    @Override
    public String toString() {
        return "CentralShop";
    }
}
