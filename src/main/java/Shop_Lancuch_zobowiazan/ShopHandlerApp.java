package Shop_Lancuch_zobowiazan;

public class ShopHandlerApp {
    public static void main(String[] args) {
        ShopRequest shopRequest = new ShopRequest("OnePlus 6T");
        ShopHandler localShopHandler = new LocalShopHandler();
        ShopHandler centralShopHandler = new CentralShopHandler();
        ShopHandler wholesaleHandler = new WholesaleHandler();

        localShopHandler.setNext(centralShopHandler);
        centralShopHandler.setNext(wholesaleHandler);

        localShopHandler.processRequest(shopRequest);

    }
}
