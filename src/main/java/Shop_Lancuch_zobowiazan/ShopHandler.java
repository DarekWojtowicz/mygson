package Shop_Lancuch_zobowiazan;



import java.util.List;

public abstract class ShopHandler {
    protected abstract List<String> getProducts();

    //deklaracja następnika
    protected ShopHandler next;

    public ShopHandler getNext() {
        return next;
    }

    public void setNext(ShopHandler next) {
        this.next = next;
    }

    //przyjmuje nasze żadanie zakupu
    protected void processRequest(ShopRequest request) {
        if (this.getProducts().contains(request.getRequest())) {
            System.out.println("Kupujesz: " + request.getRequest() + " w " + this.toString());
        } else if (next != null) {
            next.processRequest(request);
        } else {
            System.out.println("No possibility to resolve request: " + request);
        }
    }
}
