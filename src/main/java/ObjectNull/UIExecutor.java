package ObjectNull;

public class UIExecutor {

    public static void main(String[] args) {

        UIComponentFactory factory = new UIComponentFactory();
        String[] elements = {"Button", null, "button", "Checkbox", "CheckBox", "Scroll", "Auto"};

        for(String e : elements){
            UIComponent component = factory.getUIComponent(e);
            component.onClick();
        }
    }
}
