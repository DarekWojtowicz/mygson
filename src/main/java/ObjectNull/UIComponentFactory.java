package ObjectNull;

public class UIComponentFactory {

    public UIComponent getUIComponent(String type){
        if ("Button".equalsIgnoreCase(type)) {
            return new Button();
        } else if ("Checkbox".equalsIgnoreCase(type)){
            return new CheckBox();
        }
        return new NullComponent();
    }
}
