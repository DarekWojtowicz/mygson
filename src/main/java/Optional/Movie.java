package Optional;

public class Movie {

    String title;
    String description;
    int yearOfRelease;

    public Movie(String title, String description, int yearOfRelease) {
        this.title = title;
        this.description = description;
        this.yearOfRelease = yearOfRelease;
    }

    @Override
    public String toString() {
        return  title + " -> " + description + " -> " + yearOfRelease;
    }
}
