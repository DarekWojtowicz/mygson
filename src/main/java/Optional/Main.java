package Optional;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        
        Movie movie1 = new Movie("Lśnienie", "Horror", 1972);
        Movie movie2 = null;

        movie1 = Optional.ofNullable(movie1).orElseGet(() -> new Movie("Unknown", "Unknown", -1000));
        movie2 = Optional.ofNullable(movie2).orElseGet(() -> new Movie("Unknown", "Unknown", -1000));

        System.out.println(movie1);
        System.out.println(movie2);

    }

}
