package Weather;

import com.google.gson.annotations.SerializedName;

public class Weather {

    @SerializedName("coord")
    Coordinate coordinate;

    Wind wind;

    @SerializedName("main")
    Conditions conditions;

    @SerializedName("name")
    String location;

    @Override
    public String toString() {
        return "Weather{" +
                "coordinate=" + coordinate +
                ", wind=" + wind +
                ", conditions=" + conditions +
                ", location='" + location + '\'' +
                '}';
    }
}
