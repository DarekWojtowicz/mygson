package Weather;

public class Coordinate {
    float lon;
    float lat;


    public Coordinate(float lon, float lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public Coordinate() {
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "lon=" + lon +
                ", lat=" + lat +
                '}';
    }
}
