package Weather;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.function.Consumer;

public class WeatherApp {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://api.openweathermap.org/data/2.5/weather?q=Szczecin&appid=175f106adf71700578947733aa966434");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.addRequestProperty("User-Agent", "Mozilla/5.0");
        int status = con.getResponseCode();

        Gson gson = new Gson();
        BufferedReader in = new BufferedReader(
                new InputStreamReader((InputStream) con.getContent()));
        String response = readResponse(con);

        Weather weather = gson.fromJson(response, Weather.class);
        con.disconnect();


        float f = weather.conditions.temp - 273.15F;
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        String temp = String.valueOf(decimalFormat.format(f));

        Consumer<Weather> consumer = (Weather w) -> {
            System.out.println("******** POGODA ********");
            System.out.println("* dla: " + w.location + "        *");
            System.out.println("*      " + w.coordinate.lon + " st.E      *");
            System.out.println("*      " + w.coordinate.lat + " st N      *");
            System.out.println("*                      *");
            System.out.println("* temp: " + temp + " st.C.    *");
            System.out.println("*                      *");
            System.out.println("* wiatr: " + w.wind.speed + " m/s       *");
            System.out.println("* kierunek: " + w.wind.direction + " st     *");
            System.out.println("*                      *");
            System.out.println("* chmury : " + w.conditions.humidity + " [%]      *");
            System.out.println("*                      *");
            System.out.println("* ciśnienie: " + w.conditions.pressure + " hPa  *");
            System.out.println("************************");



        };
        consumer.accept(weather);

    }

    private static String readResponse(HttpURLConnection con) throws IOException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader((InputStream) con.getContent()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        return content.toString();
    }
}
