package Football;

import java.time.LocalDate;

public class FootballPlayer {

    private String name;
    private String lastName;
    private String position;
    private LocalDate dateOfBirth;

    public FootballPlayer(String name, String lastName, String position, LocalDate dateOfBirth) {
        this.name = name;
        this.lastName = lastName;
        this.position = position;
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "FootballPlayer{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", position='" + position + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPosition() {
        return position;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
}
