package Football;

import java.util.List;

public class FootballLeage {
    private String name;
    private String country;
    private int level; //poziom rozgrywek
    private List<FootballTeam> teams;

    public FootballLeage(String name, String country, int level, List<FootballTeam> teams) {
        this.name = name;
        this.country = country;
        this.level = level;
        this.teams = teams;
    }

    @Override
    public String toString() {
        return "FootballLeage{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", level=" + level +
                ", teams=" + teams +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public int getLevel() {
        return level;
    }

    public List<FootballTeam> getTeams() {
        return teams;
    }
}
