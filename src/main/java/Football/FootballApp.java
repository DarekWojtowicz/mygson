package Football;

import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class FootballApp {
    public static void main(String[] args) {

        //PL
        //Pogoń
        List<FootballPlayer> pogonPlayers = Arrays.asList
                (new FootballPlayer("Adam", "Buksa", "NA", LocalDate.of(1996, 7, 12)),
                        new FootballPlayer("Dante", "Stipica", "BR", LocalDate.of(1990, 12, 12)),
                        new FootballPlayer("Kožulj", "Kožulj", "PO", LocalDate.of(1993, 11, 15)),
                        new FootballPlayer("Michał", "Łasicki", "OB", LocalDate.of(1995, 6, 26)));

        FootballManager pogonManager = new FootballManager("Kosta", "Runjaić", "Niemcy", LocalDate.of(1971, 6, 4));
        FootballTeam pogonTeam = new FootballTeam("Pogoń", pogonPlayers, pogonManager, 71);

        //Legia
        List<FootballPlayer> legiaPlayers = Arrays.asList
                (new FootballPlayer("José", "Kanté", "NA", LocalDate.of(1990, 9, 27)),
                        new FootballPlayer("Radosław", "Majecki", "BR", LocalDate.of(1999, 11, 16)),
                        new FootballPlayer("Tomasz", "Jodłowiec", "PO", LocalDate.of(1985, 9, 8)),
                        new FootballPlayer("Lewczuk", "Lewczuk", "OB", LocalDate.of(1985, 5, 30)));

        FootballManager legiaManager = new FootballManager("Aleksandar", "Vuković", "Serbia", LocalDate.of(1979, 8, 25));
        FootballTeam legiaTeam = new FootballTeam("legia", legiaPlayers, legiaManager, 103);


        //Lech
        List<FootballPlayer> lechPlayers = Arrays.asList
                (new FootballPlayer("Paweł", "Tomczyk", "NA", LocalDate.of(1998, 5, 4)),
                        new FootballPlayer("Mickey", "van der Hart", "BR", LocalDate.of(1994, 6, 13)),
                        new FootballPlayer("Kamil", "Jóźwiak", "PO", LocalDate.of(1988, 4, 22)),
                        new FootballPlayer("Robert", "Gumny", "OB", LocalDate.of(1998, 6, 4)));

        FootballManager lechManager = new FootballManager("Dariusz", "Żuraw", "Polska", LocalDate.of(1972, 11, 14));
        FootballTeam lechTeam = new FootballTeam("Lech", lechPlayers, lechManager, 99);

        FootballLeage ekstraklasa = new FootballLeage("Ekstraklasa", "Polska", 1, Arrays.asList(pogonTeam, legiaTeam, lechTeam));

        //DE
        //Barussia
        List<FootballPlayer> borussiaPlayers = Arrays.asList
                (new FootballPlayer("Marco", "Reus", "PO", LocalDate.of(1989, 5, 31)),
                        new FootballPlayer("Roman", "Bürki", "BR", LocalDate.of(1990, 11, 14)),
                        new FootballPlayer("Paco", "Alcácer", "NA", LocalDate.of(1993, 8, 30)),
                        new FootballPlayer("Łukasz", "Piszczek", "OB", LocalDate.of(1985, 6, 3)));

        FootballManager borussiaManager = new FootballManager("Lucien", "Favre", "Szwajcaria", LocalDate.of(1957, 11, 2));
        FootballTeam barussiaTeam = new FootballTeam("Borussia", borussiaPlayers, borussiaManager, 110);

        //Bayern
        List<FootballPlayer> bayernPlayers = Arrays.asList
                (new FootballPlayer("Robert", "Lewandowski", "NA", LocalDate.of(1988, 8, 21)),
                        new FootballPlayer("Manuel", "Neuer", "BR", LocalDate.of(1986, 3, 27)),
                        new FootballPlayer("Javi", "Martínez", "PO", LocalDate.of(1988, 9, 2)),
                        new FootballPlayer("Joshua", "Kimmich", "OB", LocalDate.of(1995, 2, 8)));

        FootballManager bayernManager = new FootballManager("Niko", "Kovač", "Chorwacja", LocalDate.of(1971, 10, 15));
        FootballTeam bayernTeam = new FootballTeam("Bayern", bayernPlayers, bayernManager, 119);


        //Wolfsburg
        List<FootballPlayer> wolfsburgPlayers = Arrays.asList
                (new FootballPlayer("Wout", "Weghorst", "NA", LocalDate.of(1992, 8, 7)),
                        new FootballPlayer("Koen", "Casteels", "BR", LocalDate.of(1992, 6, 25)),
                        new FootballPlayer("Ignacio", "Camacho", "PO", LocalDate.of(1990, 5, 4)),
                        new FootballPlayer("Renato", "Steffen", "PO", LocalDate.of(1991, 11, 3)),
                        new FootballPlayer("Paul", "Verhaegh", "OB", LocalDate.of(1983, 9, 1)));

        FootballManager wolfsburgManager = new FootballManager("Oliver", "Glasner", "Austria", LocalDate.of(1974, 8, 28));
        FootballTeam wolfsburgTeam = new FootballTeam("Wolfsburg", wolfsburgPlayers, wolfsburgManager, 74);

        FootballLeage bundesliga = new FootballLeage("Bundesliga", "Niemcy", 1, Arrays.asList(barussiaTeam, bayernTeam, wolfsburgTeam));


        //ostateczna agregacja
        List<FootballLeage> europeanLeagues = Arrays.asList(ekstraklasa, bundesliga);


        //Zadania

        //1.1. Na podstawie wszystkich dostępnych lig zwróć wszystkich trenerów
        System.out.println("\n1.1. Na podstawie wszystkich dostępnych lig zwróć wszystkich trenerów\n");
        europeanLeagues.stream()
                .flatMap((FootballLeage fl) -> {
                    return fl.getTeams().stream();
                })
                .map((FootballTeam ft) -> {
                    return ft.getManager();
                })
                .forEach(System.out::println);

        //1.2. Na podstawie wszystkich dostępnych lig zwróć nazwiska wszystkich trenenerów mających co najmniej 50 lat
        System.out.println("\n1.2. Na podstawie wszystkich dostępnych lig zwróć nazwiska wszystkich trenenerów mających co najmniej 50 lat\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .map(tl -> tl.getManager())
                .filter(fm -> {
                    Period period = Period.between(fm.getDateOfBirth(), LocalDate.now());
                    int years = period.getYears();
                    return years >= 50;
                })
                .map(tm -> tm.getLastName())
                .forEach(System.out::println);

        //1.3. Na podstawie wszystkich dostępnych lig zwróć wszystkich trenenerów mających co najwyżej 45 lat
        System.out.println("\n1.3. Na podstawie wszystkich dostępnych lig zwróć wszystkich trenenerów mających co najwyżej 45 lat\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .map(ft -> ft.getManager())
                .filter(tm -> {
                    Period period = Period.between(tm.getDateOfBirth(), LocalDate.now());
                    int years = period.getYears();
                    return years <= 45;
                })
                .forEach(System.out::println);

        //1.4. Na podstawie wszystkich dostępnych lig zwróć wszystkich trenenerów narodowości niemieckiej
        System.out.println("\n1.4. Na podstawie wszystkich dostępnych lig zwróć wszystkich trenenerów narodowości niemieckiej\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .filter(ft -> "Niemcy".equalsIgnoreCase(ft.getManager().getNationality()))
                .map(ft -> ft.getManager())
                .forEach(System.out::println);

        //1.5. Na podstawie wszystkich dostępnych lig zwróć wszystkich piłkarzy
        System.out.println("\n1.5. Na podstawie wszystkich dostępnych lig zwróć wszystkich piłkarzy\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .flatMap(ft -> ft.getPlayers().stream())
                .forEach(System.out::println);

        //1.6. Na podstawie wszystkich dostępnych lig zwróć wszystkich piłkarzy - atakujacych
        System.out.println("\n1.6. Na podstawie wszystkich dostępnych lig zwróć wszystkich piłkarzy - atakujacych\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .flatMap(ft -> ft.getPlayers().stream())
                .filter(fp -> "NA".equalsIgnoreCase(fp.getPosition()))
                .forEach(System.out::println);

        //1.7.  Na podstawie wszystkich dostępnych lig zwróć imiona wszystkich piłkarzy - obrońców mających mniej niż 30 lat
        System.out.println("\n1.7.  Na podstawie wszystkich dostępnych lig zwróć imiona wszystkich piłkarzy - obrońców mających mniej niż 30 lat\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .flatMap(ft -> ft.getPlayers().stream())
                .filter(fp -> "OB".equalsIgnoreCase(fp.getPosition()))
                .filter(fp -> {
                    Period period = Period.between(fp.getDateOfBirth(), LocalDate.now());
                    int years = period.getYears();
                    return years < 30;
                })
                .map(fp -> fp.getName())
                .forEach(System.out::println);

        //1.8. Na podstawie wszystkich dostępnych lig zwróć wszystkich piłkarzy - bramkarzy mających co najmniej 33 lata
        System.out.println("\n1.8. Na podstawie wszystkich dostępnych lig zwróć wszystkich piłkarzy - bramkarzy mających co najmniej 33 lata\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .flatMap(ft -> ft.getPlayers().stream())
                .filter(fp -> "BR".equalsIgnoreCase(fp.getPosition()))
                .filter(fp -> {
                    Period period = Period.between(fp.getDateOfBirth(), LocalDate.now());
                    int years = period.getYears();
                    return years >= 33;
                })
                .forEach(System.out::println);

        //1.9. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby
        System.out.println("\n1.9. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .forEach(System.out::println);

        //1.10. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby zawierające co najmniej 5 piłkarzy
        System.out.println("\n1.10. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby zawierające co najmniej 5 piłkarzy\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .filter(ft -> ft.getPlayers().size() >= 5)
                .forEach(System.out::println);

        //1.11. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby mające trenera narodowości polskiej
        System.out.println("\n1.11. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby mające trenera narodowości polskiej\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .filter(ft -> "Polska".equalsIgnoreCase(ft.getManager().getNationality()))
                .forEach(System.out::println);

        //1.12. Na podstawie wszystkich dostępnych lig zwróć wszystkie polskie kluby
        System.out.println("\n1.12. Na podstawie wszystkich dostępnych lig zwróć wszystkie polskie kluby\n");
        europeanLeagues.stream()
                .filter(fl -> "Polska".equalsIgnoreCase(fl.getCountry()))
                .flatMap(fl -> fl.getTeams().stream())
                .forEach(System.out::println);

        //1.13. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby mające ponad 100 lat
        System.out.println("\n1.13. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby mające ponad 100 lat\n");
        europeanLeagues.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .filter(ft -> ft.getAge() > 100)
                .forEach(System.out::println);

        //1.14. Na podstawie wszystkich dostępnych lig zwróć wszystkie te które zawierają w swoich rozgrywkach co najmniej 3 zespoły
        System.out.println("\n1.14. Na podstawie wszystkich dostępnych lig zwróć wszystkie te które zawierają w swoich rozgrywkach co najmniej 3 zespoły\n");
        europeanLeagues.stream()
                .filter(fl -> fl.getTeams().size() >= 3)
                .forEach(System.out::println);

        //1.15. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby pierwszoligowe (level == 1)
        System.out.println("\n1.15. Na podstawie wszystkich dostępnych lig zwróć wszystkie kluby pierwszoligowe (level == 1)\n");
        europeanLeagues.stream()
                .filter(fl -> fl.getLevel() == 1)
                .flatMap(fl -> fl.getTeams().stream())
                .forEach(System.out::println);

        System.out.println("\nWyszukiwanie klubów\n");

        Optional<FootballTeam> myFoundTeamOptional = findTeamOfManager(europeanLeagues, pogonManager);
        FootballTeam myFoundTeam = myFoundTeamOptional.orElseThrow(() -> new NoSuchElementException("Nothing to show"));
        System.out.println(myFoundTeam);

        /*
        FootballManager fakeManager = null;
        myFoundTeamOptional = findTeamOfManager(europeanLeagues, fakeManager);
        myFoundTeam = myFoundTeamOptional.orElseThrow(() -> new NoSuchElementException("Nothing to show"));
        System.out.println(myFoundTeam);
        */

        myFoundTeamOptional = findGermanTeam(europeanLeagues);
        myFoundTeam = myFoundTeamOptional.orElseThrow(() -> new NoSuchElementException("Nothing to show"));
        System.out.println(myFoundTeam);


    }

    static Optional<FootballTeam> findTeamOfManager(List<FootballLeage> list, FootballManager footballManager) {
        return list.stream()
                .flatMap(fl -> fl.getTeams().stream())
                .filter(ft -> ft.getManager() == footballManager)
                .findAny();
    }

    static Optional<FootballTeam> findGermanTeam(List<FootballLeage> list) {
        return list.stream()
                .filter(fl -> "Niemcy".equalsIgnoreCase(fl.getCountry()))
                .flatMap(fl -> fl.getTeams().stream())
                .findAny();
    }
}
