package Football;

import java.util.List;

public class FootballTeam {
    private String name;
    private List<FootballPlayer> players;
    private FootballManager manager;
    private int age; //wiek klubu

    public FootballTeam(String name, List<FootballPlayer> players, FootballManager manager, int age) {
        this.name = name;
        this.players = players;
        this.manager = manager;
        this.age = age;
    }

    @Override
    public String toString() {
        return "FootballTeam{" +
                "name='" + name + '\'' +
                ", players=" + players +
                ", manager=" + manager +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public List<FootballPlayer> getPlayers() {
        return players;
    }

    public FootballManager getManager() {
        return manager;
    }

    public int getAge() {
        return age;
    }
}
