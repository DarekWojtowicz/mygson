package Polecenie;

public interface Command {
    public void execute();
}
