package Polecenie;

public class TvVolumeUpCommand implements Command {
    TV tv;

    public TvVolumeUpCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.increaseVolume();
    }
}
