package Polecenie;

public class TvVolumeDownCommand implements Command {
    TV tv;

    public TvVolumeDownCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.decreaseVolume();
    }
}
