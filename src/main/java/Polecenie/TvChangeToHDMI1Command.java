package Polecenie;

public class TvChangeToHDMI1Command implements Command {
    TV tv;

    public TvChangeToHDMI1Command(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.switchToHDMI1();
    }
}
