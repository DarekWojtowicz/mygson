package Polecenie;

public class TvChangeToHDMI2Command implements Command {
    TV tv;

    public TvChangeToHDMI2Command(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.switchToHDMI2();
    }
}
