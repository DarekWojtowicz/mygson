package Polecenie;

public class Main {
    public static void main(String[] args) {
        TV tv = new TV();
        RemoteControler remote = new RemoteControler();
        System.out.println(tv);
        Command onTV = new TvOnCommand(tv);
        Command offTV = new TvOffCommand(tv);
        Command setHDM1 = new TvChangeToHDMI1Command(tv);
        Command setHDM2 = new TvChangeToHDMI2Command(tv);
        Command volumeUp = new TvVolumeUpCommand(tv);
        Command volumeDown = new TvVolumeDownCommand(tv);

        remote.setCommand(onTV);
        remote.pressButton();
        System.out.println(tv);

        remote.setCommand(setHDM2);
        remote.pressButton();
        System.out.println(tv);

        remote.setCommand(volumeUp);
        remote.pressButton();
        System.out.println(tv);






    }
}
