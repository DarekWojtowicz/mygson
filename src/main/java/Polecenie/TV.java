package Polecenie;

public class TV {
    private HDMI hdmi;
    private boolean isON;
    private int volume = 50;

    public void switchOn() {
        isON = true;
    }

    public void switchOff() {
        isON = false;
    }

    public void switchToHDMI1() {
        hdmi = HDMI.HDMI1;
    }

    public void switchToHDMI2() {
        hdmi = HDMI.HDMI2;
    }

    public void increaseVolume() {
        if (volume < 100) {
            volume++;
        }
    }

    public void decreaseVolume() {
        if (volume > 0) {
            volume--;
        }
    }

    public HDMI getHdmiState() {
        return hdmi;
    }

    public boolean isON() {
        return isON;
    }

    public int getVolumeState() {
        return volume;
    }

    @Override
    public String toString() {
        return "TV{" +
                "hdmi=" + hdmi +
                ", isON=" + isON +
                ", volume=" + volume +
                '}';
    }
}
