package CodeWars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class codewars {
    public static void main(String[] args) {
        int[] a = {-1, 121, 144, 19, 161, 19, 144, 19, 11};
        int[] b = {1,121, 14641, 20736, 361, 25921, 361, 20736, 361};

        List <Integer> lA  = Arrays.stream(a)
                .boxed()
                .sorted()
                .collect(Collectors.toList());
        List <Integer> lB = Arrays.stream(b)
                .boxed()
                .sorted()
                .collect(Collectors.toList());

        lA = lA.stream()
                .map(i -> i*i)
                .collect(Collectors.toList());

       // Collections.sort(lA);
        System.out.println(lA.equals(lB));

//rozwiązanie z CodeWars

        System.out.println(Arrays.equals(Arrays.stream(a).map(i -> i * i).sorted().toArray(), Arrays.stream(b).sorted().toArray()));



        System.out.println(lA.toString());
        System.out.println(lB.toString());


        boolean exit = false;

        if (a != null && b != null && a.length == b.length) {
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < b.length; j++) {
                    if (a[j] * a[j] == b[i]) {
                        a[j] = -1;
                        b[i] = -1;
                        break;
                    }
                }
            }
        } else {
            exit = false;
        }

        for(int i = 0; i < a.length; i++){
            if (a[i] == -1 && b[i] == -1){
                exit = true;
            } else {
                exit = false;
            }
        }
        System.out.println(exit);

    }
}
