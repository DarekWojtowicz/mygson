package CodeWars;

import java.util.Arrays;

public class Zada_27_08 {

    public static void main(String[] args) {
        int[] q = {4,4,5,1,5,5,6,4,6,3,5,5,3,2,2,5,3,4,5};  //kolejka klientów z zakupami
        int no = 5;                                        // ilosc kas

        System.out.println(solveSuperMarketQueue(q, no));

    }

    public static int solveSuperMarketQueue(int[] customers, int n) {
        int[] count = new int[n];

        for (int j = 0; j < customers.length; j++) {
            count[0] += customers[j];
            Arrays.sort(count);
            }
        return count[n - 1];        //czas obsługi
    }
}
