package CodeWars;

import java.util.Arrays;

public class MyFirstInterpreter {
    public String output;
    public String code;

    public MyFirstInterpreter(String code) {

        this.code = code;

    }
    public String interpret() {

        code = code.replaceAll("[^+.]","");
        code = code +"+";
        String[] codedArray = code.split("\\.");
        int valueOfCell = 0;
        char[] outputChars = new char[codedArray.length - 1];

        for (int i = 0; i < codedArray.length - 1; i++) {
            valueOfCell += codedArray[i].length();
            if(valueOfCell > 255) {
                valueOfCell-= 256;
            }
            outputChars[i] = (char) valueOfCell;
        }
        output = new String(outputChars);
        return output;
    }
}
