package CodeWars;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StripComments {

    public static String stripComments(String text, String[] commentSymbols) {
        int quantityOfSymbols = commentSymbols.length;
        String regex = "";

        for (int i = 0; i < quantityOfSymbols; i++) {
            regex = commentSymbols[i] + ".*?(\\n|$)";
            text = text.replaceAll(regex, "\n");
        }

        return text;
    }
}
