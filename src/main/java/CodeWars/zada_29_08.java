package CodeWars;

import java.util.*;
import java.util.stream.Collectors;

public class zada_29_08 {
    public static void main(String[] args) {
    String str = "abcd";
    List<String> list = new ArrayList();

     singlePermutations(str).stream()
                .forEach(s -> {
                    System.out.println(s);
                });



    }

    public static List<String> singlePermutations(String s) {
        if (s.length() == 0)
            return null;
        int length = fact(s.length());
        StringBuilder[] sb = new StringBuilder[length];
        for (int i = 0; i < length; i++) {
            sb[i] = new StringBuilder();
        }
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            int times = length / (i + 1);
            for (int j = 0; j < times; j++) {
                for (int k = 0; k < length / times; k++) {
                    sb[j * length / times + k].insert(k, ch);
                }
            }
        }

        String[] str = new String[sb.length];
        for (int i = 0; i < sb.length; i++)
            str[i] = sb[i].toString();

        List<String> list = Arrays.asList(str).stream()
                .distinct()
                .collect(Collectors.toList());

        return list;
    }


    public static int fact(int i){

        if(i == 0){
            return 1;
        }else {
            return i * fact(i-1);
        }
    }
}
