package SerializacjaZadanie;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import java.util.Arrays;
import java.util.StringTokenizer;


public class Main {


    class Book {

        String title;
        String isbn;
        @SerializedName("year_of_release")
        int yeraOfRelease;
        float price;
        String[] authors;

        @Override
        public String toString() {
            return "Book{" +
                    "title='" + title + '\'' +
                    ", isbn='" + isbn + '\'' +
                    ", yeraOfRelease=" + yeraOfRelease +
                    ", price=" + price +
                    ", authors=" + Arrays.toString(authors) +
                    '}';
        }
    }


    public static void main(String[] args) {
        String enterJson = "{\n" +
                "  \"title\": \"Java Puzzlers: Traps, Pitfalls, and Corner Cases\",\n" +
                "  \"isbn\": \"032133678X\",\n" +
                "  \"year_of_release\": 2000,\n" +
                "  \"price\": 34.50,\t\n" +
                "  \"authors\": [\n" +
                "    \"Joshua Bloch\",\n" +
                "    \"Neal Gafter\"\n" +
                "  ]\n" +
                "}\n";

        Gson gson = new Gson();
        Book book = gson.fromJson(enterJson, Book.class);
        System.out.println(book);
    }
}




