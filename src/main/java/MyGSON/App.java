package MyGSON;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        Gson gson = new Gson();

        Movie movie1 = new Movie("Lśnienie", "Horror", "1972", new String[]{"PL", "DE", "EN"});
        Movie movie2 = new Movie("King size", "Komedia", "1988", new String[]{"PL", "EN"});
        Movie movie3 = new Movie("Solaris", "S-F", "2014", new String[]{"PL", "DE"});


        List<Movie> listOfMovie = new ArrayList<>();
        List<Movie> incomingList = new ArrayList<>();

        listOfMovie.add(movie1);
        listOfMovie.add(movie2);
        listOfMovie.add(movie3);


        // Serializacja
        String json1 = gson.toJson(listOfMovie);
        System.out.println("JSON: " + json1);


        // Deserializacja
        Type listType = new TypeToken<ArrayList<Movie>>() {
        }.getType();
        List<Movie> zJason = gson.fromJson(json1, listType);
        zJason.stream()
                .forEach(System.out::println);

    }
}
