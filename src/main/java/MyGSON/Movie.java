package MyGSON;

import java.util.Arrays;

public class Movie {
    String title;
    String genre;
    String year;
    String[] langs;

    public Movie(String title, String genre, String year, String[] langs) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.langs = langs;
    }

    public Movie() {
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", year='" + year + '\'' +
                ", langs=" + Arrays.toString(langs) +
                '}';
    }
}
