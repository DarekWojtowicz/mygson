package Weather;

import com.google.gson.annotations.SerializedName;

public class Conditions {

    float temp;
    int pressure;
    int humidity;

    @SerializedName("temp_max")
    float maxtemp;

    @SerializedName("temp_min")
    float minTemp;


    @Override
    public String toString() {
        return "Conditions{" +
                "temp=" + temp +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", maxtemp=" + maxtemp +
                ", minTemp=" + minTemp +
                '}';
    }
}
