package Weather;

import com.google.gson.annotations.SerializedName;

public class Wind {

    float speed;

    @SerializedName("deg")
    int direction;

    @Override
    public String toString() {
        return "Wind{" +
                "speed=" + speed +
                ", direction=" + direction +
                '}';
    }
}
